package com.example.rongweather.place

import com.google.gson.annotations.SerializedName
import org.litepal.crud.LitePalSupport


class Country (@SerializedName("name") val countryName:String,@SerializedName("weather_id") val weatherId:String):LitePalSupport() {
    @Transient val id = 0
    var cityId = 0
}